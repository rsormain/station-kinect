package securite;

public class GenerateurCleControle { //génère une chaîne de caractère qui est également calculée par le serveur et qui permet de vérifier la validité de la transaction

	public static String getCleControleur(Session session, int coins, int repetitions, long duree){
		
		String cle = Integer.toString(coins) + Integer.toString(repetitions) + Long.toString(duree) + Integer.toString(session.getNmbExercices());
		
		return Md5.encode(cle);
		
	}
}
