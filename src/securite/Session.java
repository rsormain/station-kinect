package securite;

import donnees.User;

public class Session { 	//encapsule l'objet session avec, en plus de l'identifiant de session (uuid), 
						// le nombre d'exercices réalisés dans la session afin de contrôler la conformité niveau serveur
	

	private String uuid;
	private int nmbExercices;
	
	public Session(String uuid) {
		this.uuid = uuid;
		nmbExercices=0;
	}

	public void incrementNmbExercices(){
		nmbExercices++;
	}
	
	
	public int getNmbExercices() {
		return nmbExercices;
	}

	public String getUuid() {
		return uuid;
	}

	
}
