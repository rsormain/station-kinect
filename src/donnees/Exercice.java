package donnees;

import java.util.Date;



public class Exercice {
	private Long id;
	private Date date;
	private String type;
	private int duree;
	private int repetitions;
	private int coins;
	
	public Long getId() {
		return id;
	}

	public Exercice(){}
	
	public Exercice(Date date, String type, int duree, int repetitions, int coins) {
		this.date = date;
		this.type = type;
		this.duree = duree;
		this.repetitions = repetitions;
		this.coins = coins;
	}


	public Date getDate() {
		return date;
	}
	public String getType() {
		return type;
	}
	public int getDuree() {
		return duree;
	}
	public int getRepetitions() {
		return repetitions;
	}
	public int getCoins() {
		return coins;
	}
	
	
}
