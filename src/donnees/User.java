package donnees;

import java.util.ArrayList;
import java.util.Date;


public class User {

	private String id;
	private String nom;
	private String prenom;
	private Date inscription;
	private int coins;
	private ArrayList<String> friends = new ArrayList<String>();
	private String urlPhoto;
	
	public String getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public Date getInscription() {
		return inscription;
	}
	public int getCoins() {
		return coins;
	}
	public ArrayList<String> getFriends() {
		return friends;
	}
	public String getUrlPhoto() {
		return urlPhoto;
	}
	
	
	
	
}
