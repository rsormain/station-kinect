package donnees;

import java.util.ArrayList;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class Deserialiseur {

	
	public static Exercice deSerialiseExercice(String xml) {
		
		XStream xstream = new XStream(new DomDriver("UTF-8"));
		xstream.alias("Exercice", Exercice.class);
		Exercice exercice = (Exercice) xstream.fromXML(xml);

		return exercice;
		
	}
	
public static ArrayList<Exercice> deSerialiseExercices(String xml) {
		
		XStream xstream = new XStream(new DomDriver("UTF-8"));
		xstream.alias("Exercice", Exercice.class);
		ArrayList<Exercice> exercices = (ArrayList<Exercice>) xstream.fromXML(xml);

		return exercices;
		
	}
	
	
	public static User deSerialiseUser(String xml) {
	
		XStream xstream = new XStream(new DomDriver("UTF-8"));
		xstream.alias("User", User.class);
		User user = (User) xstream.fromXML(xml);

		return user;
		
	}
}
