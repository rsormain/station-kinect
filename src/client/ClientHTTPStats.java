package client;

import java.io.*;
import java.net.*;

public class ClientHTTPStats {  //permet d'obtenir le nombre de secondes passées par l'utilisateur sur le sport "type"

	public static String getSecondes(String session, String type) {
		
		String surl = "http://pact43fittogether.appspot.com/Stats"; 
		String charset = "UTF-8"; //on définit le codage des caractères
		try {
		String query = String.format("type=%s&session=%s",  URLEncoder.encode(type, charset), URLEncoder.encode(session, charset)); // on s'assure que les chaînes sont bien encodées 
	
			
			URLConnection connection = new URL(surl + "?" + query).openConnection();
			connection.setRequestProperty("Accept-Charset", charset);
			InputStream reponse = connection.getInputStream();

			//recuperation de la chaine de caractères (a priori, une seule ligne)
			BufferedReader rd = new BufferedReader(new InputStreamReader(reponse, charset));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				response.append(line);
			}
			rd.close();
			return response.toString();

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
		
		
		

	}
}
