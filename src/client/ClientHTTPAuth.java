package client;

import java.io.*;
import java.net.*;

public class ClientHTTPAuth {  //permet d'authentifier l'utilisateur 

	public static String connection(String id, String password) {
		
		String surl = "http://pact43fittogether.appspot.com/Auth"; 
		String charset = "UTF-8"; //on définit le codage des caractères
		String idmachine = "machine1"; //identifiant de la machine (TODO à développer) chaque application devra avoir sa "license", ce qui peremt de contrôler l'ajout de Sportcoins à un compte
		String type = "kinect"; //type de session à ouvrir
		
		try {
		String query = String.format("id=%s&password=%s&type=%s&m=%s", 
		     URLEncoder.encode(id, charset), 
		     URLEncoder.encode(password, charset),
		     URLEncoder.encode(type, charset),
		     URLEncoder.encode(idmachine, charset)); // on s'assure que les chaînes sont bien encodées 
	
			URL url = new URL(surl);
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			OutputStream output = httpConnection.getOutputStream(); //envoi de la requête
			try {
			     output.write(query.getBytes(charset));
			} finally {
			     try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
			if (httpConnection.getResponseCode() != 200) {
			
				return null;
			}
			//recuperation du numéro de session
			BufferedReader s = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			String reponse = s.readLine();
			s.close();
			httpConnection.disconnect();
			return reponse;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
		
		
		

	}
}
