package client;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import donnees.Exercice;

public class ClientHTTPNouvelExercice {  //permet d'ajouter un exercice à l'historique de l'utilisateur et d'augmenter son nombre de sportcoins

	public static void addExercice(String session, Exercice exercice, String controle) {
		
		String surl = "http://pact43fittogether.appspot.com/NouvelExercice"; 
		String charset = "UTF-8"; //on définit le codage des caractères
		
		
		try {
			
			SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
			sf.format(new Date());
		String query = String.format("session=%s&coins=%s&duree=%s&repetitions=%s&sport=%s&c=%s", 
		     URLEncoder.encode(session, charset), 
		     URLEncoder.encode(Integer.toString(exercice.getCoins()), charset),
		     URLEncoder.encode(Long.toString(exercice.getDuree()), charset),
		     URLEncoder.encode(Integer.toString(exercice.getRepetitions()), charset),
		     URLEncoder.encode(exercice.getType(), charset),
		     URLEncoder.encode(controle, charset)); // on s'assure que les chaînes sont bien encodées 
	
			URL url = new URL(surl);
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			OutputStream output = httpConnection.getOutputStream(); //envoi de la requête
			try {
			     output.write(query.getBytes(charset));
			} finally {
			     try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
			
			int code = httpConnection.getResponseCode();
			
			
			httpConnection.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		

	}
}
