package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class ClientHTTPLogout {

public static String deconnection(String session) {
		
		String surl = "http://pact43fittogether.appspot.com/Logout"; 
		String charset = "UTF-8"; //on définit le codage des caractères
		try {
		String query = String.format("session=%s", URLEncoder.encode(session, charset)); // on s'assure que les chaînes sont bien encodées 
	
			
			URLConnection connection = new URL(surl + "?" + query).openConnection();
			connection.setRequestProperty("Accept-Charset", charset);
			InputStream reponse = connection.getInputStream();
			
			//on récupère un éventuel code d'erreur
			BufferedReader rd = new BufferedReader(new InputStreamReader(reponse, charset));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
		
		
		

	}
}
