package gui;

import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import securite.Session;
import client.ClientHTTPInfoExercices;
import client.ClientHTTPInfoUser;
import donnees.Deserialiseur;
import donnees.Exercice;
import donnees.User;

public class AccueilController { //fournit les infos nécessaire à l'affichage (réalisé par FentreAcceuil)
	
	private Session session; //numéro de session
	private String infoXML; //information sur l'utilisateur
	private User user;
	private Image imageUser;
	
	public AccueilController(String s){
  		
  		infoXML = ClientHTTPInfoUser.connection(s);
  		user = Deserialiseur.deSerialiseUser(infoXML);
  		session = new Session(s);
  		imageUser = null;
  		URL url;
  		if (user.getUrlPhoto() != null){
  			try {
  				url = new URL(user.getUrlPhoto() + "=s110"); //l'image reçue est directement réduite à 110pixels
  				imageUser = ImageIO.read(url);
  			} catch (MalformedURLException e1) {
  				e1.printStackTrace();
  			} catch (IOException e) {
  				e.printStackTrace();
  			}
  		}
	}
	
	public AccueilController(Session session2) {
		this(session2.getUuid());
		session = session2;
	}

	public Exercice getLastExercice(){
		Exercice lastExercice = Deserialiseur.deSerialiseExercice(ClientHTTPInfoExercices.connection(session.getUuid(), "last"));
		return lastExercice;
	}
	
	public User getUser(){
		return user;
	}
	
	public Session getSession(){
		return session;
	}

	
	
	public Image getImageUser(){
		return imageUser;
	}
}
