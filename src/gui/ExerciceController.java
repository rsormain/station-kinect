package gui;

import securite.Session;

public class ExerciceController {
	
	private Session session;
	private String type; // type de l'exercice
	
	public ExerciceController(Session session, String type){
		this.session = session;
		this.type = type;
	}

	public Session getSession() {
		return session;
	}

	public String getType() {
		return type;
	}
	
	public void goToResultats( int totalsecondes, int repetitions){
		
		ResultatsController rController = new ResultatsController(session, type, totalsecondes, repetitions);
		FenetreResultats fresultats = new FenetreResultats(rController);
		fresultats.setVisible(true);
		
	}
	

}
