package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import net.miginfocom.swing.MigLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import securite.Session;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FenetreExercice extends JFrame {

	private JPanel contentPane;
	private static int centiseconde=0,minute=0,seconde=0,totalsecondes=0;
	private JTextField textField;

	public FenetreExercice(final ExerciceController eController) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNousSommesPrts = new JLabel("Nous sommes prêts à commencer l'exercice");
		lblNousSommesPrts.setFont(FontesLoader.getFonteTitre((float)30, this));
		panel.add(lblNousSommesPrts);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new MigLayout("", "[272.00][261.00,grow]", "[152.00,grow][157.00,grow][97.00,grow]"));
		
		
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, "cell 1 0,grow");
		panel_2.setLayout(new BorderLayout(0, 0));
		
		
		// --------------------CHRONOMETRE------------------------------------
		
		final JLabel label = new JLabel(minute+":"+seconde+":"+centiseconde);
		label.setFont(FontesLoader.getFonteTexte((float)70, this));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(label, BorderLayout.CENTER);
		
		 ActionListener tache_timer= new ActionListener()
		{
			public void actionPerformed(ActionEvent e1)
			{
				centiseconde++;
				if(centiseconde==99)
				{
					centiseconde=0;
					seconde++;
					totalsecondes++;
				}
				if(seconde==59)
				{
					seconde=0;
					minute++;
				}
				label.setText(minute+":"+seconde+":"+centiseconde);/* rafraichir le label */
			}
		};
		
		final Timer timer1= new Timer(10,tache_timer); //10 donne" un centième de seconde avant répétition
		
		// ---------------------------------------------------------------------
		
		JButton btnNewButton = new JButton("Commencer l'exercice");
		btnNewButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				timer1.start();
			}
		});
		btnNewButton.setBackground(new Color(153, 204, 102));
		btnNewButton.setFont(FontesLoader.getFonteTitre((float)24, this));
		
		
		panel_1.add(btnNewButton, "cell 0 0,alignx center");
		
		JLabel lblNewLabel = new JLabel("Image descriptive du mouvement");
		panel_1.add(lblNewLabel, "cell 0 1,alignx center,aligny center");
		
		JLabel lblConseilsEnTout = new JLabel("Conseils en tout genre");
		lblConseilsEnTout.setFont(FontesLoader.getFonteTexte((float)15, this));
		panel_1.add(lblConseilsEnTout, "cell 1 1,alignx center,aligny center");
		
		
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, "cell 1 2,grow");
		panel_4.setLayout(new MigLayout("", "[grow]", "[grow][grow]"));
		
		JLabel lblNombreDeRptitions = new JLabel("Nombre de répétitions :");
		lblNombreDeRptitions.setFont(FontesLoader.getFonteTexte((float)20, this));
		panel_4.add(lblNombreDeRptitions, "cell 0 0,alignx center");
		
		textField = new JTextField();
		textField.setFont(FontesLoader.getFonteTexte((float)20, this));
		panel_4.add(textField, "cell 0 1,alignx center");
		textField.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(153, 204, 102));
		contentPane.add(panel_3, BorderLayout.SOUTH);
		
		JButton btnTerminerLexercice = new JButton("Terminer l'exercice");
		btnTerminerLexercice.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				timer1.stop();
				String repetitions = textField.getText();
				if (repetitions == null){repetitions ="0";}
				eController.goToResultats(totalsecondes, Integer.parseInt(repetitions));
				
				setVisible(false);
				dispose();
				
			}
		});
		btnTerminerLexercice.setBackground(new Color(255, 0, 0));
		btnTerminerLexercice.setFont(FontesLoader.getFonteTitre((float)24, this));
		panel_1.add(btnTerminerLexercice, "cell 0 2,alignx center");
		
		JButton btnNewButton_1 = new JButton("Retour à l'accueil");
		btnNewButton_1.setFont(FontesLoader.getFonteTitre((float)18, this));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				timer1.stop();
				AccueilController AController = new AccueilController(eController.getSession()); //on ouvre la page d'acceuil avec la même session
				FenetreAccueil fenetreAccueil = new FenetreAccueil(AController);
				
				fenetreAccueil.setVisible(true);
				
				setVisible(false);  // on désactive l'affichage de la fenêtre de connection et on libère ses ressources. 
				dispose();
				
			}
		});
		panel_3.setLayout(new BorderLayout(0, 0));
		panel_3.add(btnNewButton_1, BorderLayout.EAST);
	}

}
