package gui;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.JFrame;

public class FontesLoader { //fournit les différentes polices d'affichages

	public static Font getFonteTitre(float size, JFrame jf){

		InputStream is = jf.getClass().getResourceAsStream("/fonts/BebasNeue.otf");
		Font fonteTitre = null;
		try {
			fonteTitre = Font.createFont(Font.TRUETYPE_FONT,is);
			fonteTitre = fonteTitre.deriveFont(size);
			fonteTitre = fonteTitre.deriveFont(Font.PLAIN);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}


		return fonteTitre;

	}
	
	public static Font getFonteTexte(float size, JFrame jf){

		InputStream is = jf.getClass().getResourceAsStream("/fonts/Sertig_0.otf");
		Font fonteTitre = null;
		try {
			fonteTitre = Font.createFont(Font.TRUETYPE_FONT,is);
			fonteTitre = fonteTitre.deriveFont(size);
			fonteTitre = fonteTitre.deriveFont(Font.PLAIN);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}


		return fonteTitre;

	}
	
	public static Font getFonteTexteGras(float size, JFrame jf){

		InputStream is = jf.getClass().getResourceAsStream("/fonts/Sertig_0.otf");
		Font fonteTitre = null;
		try {
			fonteTitre = Font.createFont(Font.TRUETYPE_FONT,is);
			fonteTitre = fonteTitre.deriveFont(size);
			fonteTitre = fonteTitre.deriveFont(Font.BOLD);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}


		return fonteTitre;

	}


}
