package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;

import java.awt.Color;

import javax.swing.JButton;

import securite.Session;
import client.ClientHTTPLogout;
import donnees.Exercice;
import donnees.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Insets;

import net.miginfocom.swing.MigLayout;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;

public class FenetreAccueil extends JFrame {

	private JPanel contentPane;

	
	public FenetreAccueil(final AccueilController controller) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 796, 609);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Bienvenue !");
		lblNewLabel.setFont(FontesLoader.getFonteTitre((float)37, this));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 204, 102));
		contentPane.add(panel_1, BorderLayout.WEST);
		panel_1.setLayout(new MigLayout("", "[:120.00px:150px]", "[62.00px][:84.00px:100px,fill][65.00px]"));
		
		JLabel lblJeanDupond = new JLabel(controller.getUser().getPrenom() + " " + controller.getUser().getNom());
		lblJeanDupond.setFont(FontesLoader.getFonteTexteGras((float)22, this));
		panel_1.add(lblJeanDupond, "cell 0 0,alignx center,growy");
		
		JLabel lblNewLabel_3 = new JLabel("");
		if (controller.getImageUser() == null){
			lblNewLabel_3.setIcon(new ImageIcon(FenetreAccueil.class.getResource("/images/profile.png")));
		}
		else {lblNewLabel_3.setIcon(new ImageIcon(controller.getImageUser())); }
		lblNewLabel_3.setFont(new Font("Tahoma", Font.ITALIC, 13));
		panel_1.add(lblNewLabel_3, "cell 0 1,alignx center,aligny center");
		
		JLabel lblSportCoins = new JLabel(controller.getUser().getCoins() + " SportCoins");
		lblSportCoins.setFont(FontesLoader.getFonteTexte((float)24, this));
		panel_1.add(lblSportCoins, "cell 0 2,alignx center,growy");
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnDconnexion = new JButton("Déconnexion");
		btnDconnexion.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				ClientHTTPLogout.deconnection(controller.getSession().getUuid());
				JOptionPane jop1 = new JOptionPane();
				jop1.showMessageDialog(null, "Déconnexion réussie, à bientôt", "Information", JOptionPane.INFORMATION_MESSAGE);
				FenetreConnexion fenetreConnexion = new FenetreConnexion(); //on ouvre une nouvelle fenêtre de connection 
				fenetreConnexion.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		
		btnDconnexion.setForeground(new Color(153, 0, 0));
		btnDconnexion.setFont(FontesLoader.getFonteTitre((float)26, this));
	
		panel_2.setLayout(new BorderLayout(0, 0));
		panel_2.add(btnDconnexion, BorderLayout.EAST);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(153, 204, 102));
		contentPane.add(panel_3, BorderLayout.EAST);
		
		JLabel lblNewLabel_1 = new JLabel("Dernières nouvelles :");
		lblNewLabel_1.setFont(FontesLoader.getFonteTexte((float)20, this));
		panel_3.add(lblNewLabel_1);
		
		JPanel panel_4 = new JPanel();
		contentPane.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JLabel lblVotreDernirePerformance = new JLabel(" Votre dernière performance :");
		lblVotreDernirePerformance.setFont(FontesLoader.getFonteTexte((float)22, this));
		panel_6.add(lblVotreDernirePerformance, BorderLayout.NORTH);
		
		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8, BorderLayout.CENTER);
		panel_8.setLayout(new MigLayout("", "[480.00,grow]", "[][][]"));
		
		Font texte22 = FontesLoader.getFonteTexte((float)22, this);
		
		Exercice lastExercice = controller.getLastExercice();
		if( lastExercice != null ){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			JLabel lblLeVous = new JLabel("Le " + sdf.format(lastExercice.getDate()) + ", vous avez effectué :");
			lblLeVous.setFont(texte22);
			panel_8.add(lblLeVous, "cell 0 0,alignx center");

			JLabel lblNewLabel_2 = new JLabel(lastExercice.getRepetitions() +" "+ lastExercice.getType() + " en " + 
								(int) ((lastExercice.getDuree())/60)+ " minutes et " + (int) ((lastExercice.getDuree())%60)+ " secondes");
			lblNewLabel_2.setFont(texte22);
			panel_8.add(lblNewLabel_2, "cell 0 1,alignx center");

			JLabel lblBravoVous = new JLabel("Bravo ! Vous avez gagné "+ lastExercice.getCoins() +" SportCoins");
			lblBravoVous.setFont(texte22);
			panel_8.add(lblBravoVous, "cell 0 2,alignx center");
		}
		else{
			JLabel lblPasEncore = new JLabel("Vous n'avez encore fait aucun exercice");
			lblPasEncore.setFont(texte22);
			panel_8.add(lblPasEncore, "cell 0 1,alignx center");
			
			JLabel lblCommencez = new JLabel("Commencez à utiliser FitTogether dès maintenant !");
			lblCommencez.setFont(texte22);
			panel_8.add(lblCommencez, "cell 0 2,alignx center");
		}
		
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JLabel lblQuelEntranementVou = new JLabel(" Choisissez un exercice à pratiquer :");
		lblQuelEntranementVou.setFont(texte22);
		panel_5.add(lblQuelEntranementVou, BorderLayout.NORTH);
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7, BorderLayout.CENTER);
		GridBagLayout gbl_panel_7 = new GridBagLayout();
		gbl_panel_7.columnWidths = new int[]{129, 111, 0};
		gbl_panel_7.rowHeights = new int[]{52, 45, 0};
		gbl_panel_7.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_7.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panel_7.setLayout(gbl_panel_7);
		
		Font titre40 = FontesLoader.getFonteTitre((float)40, this);
		
		JButton btnCiseaux = new JButton("Ciseaux");
		btnCiseaux.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				ExerciceController eController = new ExerciceController(controller.getSession(), "ciseaux");
				FenetreExercice fExercice = new FenetreExercice(eController);
				fExercice.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			
			}
		});
		
		btnCiseaux.setFont(titre40);
		GridBagConstraints gbc_btnCiseaux = new GridBagConstraints();
		gbc_btnCiseaux.insets = new Insets(0, 0, 5, 5);
		gbc_btnCiseaux.gridx = 0;
		gbc_btnCiseaux.gridy = 0;
		panel_7.add(btnCiseaux, gbc_btnCiseaux);
		
		JButton btnBoxe = new JButton("Boxe");
		btnBoxe.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				ExerciceController eController = new ExerciceController(controller.getSession(), "boxe");
				FenetreExercice fExercice = new FenetreExercice(eController);
				fExercice.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		btnBoxe.setFont(titre40);
		GridBagConstraints gbc_btnBoxe = new GridBagConstraints();
		gbc_btnBoxe.insets = new Insets(0, 0, 5, 0);
		gbc_btnBoxe.gridx = 1;
		gbc_btnBoxe.gridy = 0;
		panel_7.add(btnBoxe, gbc_btnBoxe);
		
		JButton btnNewButton = new JButton("Pompes");
		btnNewButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				ExerciceController eController = new ExerciceController(controller.getSession(), "pompes");
				FenetreExercice fExercice = new FenetreExercice(eController);
				fExercice.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		btnNewButton.setFont(titre40);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 1;
		panel_7.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Squat");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				ExerciceController eController = new ExerciceController(controller.getSession(), "squat");
				FenetreExercice fExercice = new FenetreExercice(eController);
				fExercice.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		btnNewButton_1.setFont(titre40);
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 1;
		panel_7.add(btnNewButton_1, gbc_btnNewButton_1);
	}

}
