package gui;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import javax.imageio.ImageIO;

import client.ClientHTTPNouvelExercice;
import client.ClientHTTPStats;
import securite.GenerateurCleControle;
import securite.Session;
import utils.CalculateurSportCoins;
import donnees.Exercice;

public class ResultatsController {

	private Exercice exercice;
	private Session session;
	
	public ResultatsController(Session session, String type, int duree, int repetitions){
		
		this.session = session;
		String sTotalSecondes = ClientHTTPStats.getSecondes(session.getUuid(), type);
		
		long totalSecondes = Long.parseLong(sTotalSecondes);
		
		int SportCoins = CalculateurSportCoins.getSportCoins(type, duree, repetitions, totalSecondes);
		
		exercice = new Exercice(new Date(), type, duree, repetitions, SportCoins);
		
		String clecontrole = GenerateurCleControle.getCleControleur(session, SportCoins, repetitions, duree);
		
		ClientHTTPNouvelExercice.addExercice(session.getUuid(), exercice, clecontrole);
		
		session.incrementNmbExercices(); // on incrémente le nombre d'exercices réalisés sur cette session
	}
	
	public int getCoins(){
		return exercice.getCoins();
	}
	
	public Image getLogo(int largeur, URL url){
	
		int hauteur =  (int) (largeur * (570.0 / 719.0) ); 
		try {
			Image image = ImageIO.read(url);
			return image.getScaledInstance(largeur, hauteur, Image.SCALE_DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public int getRepetitions(){
		return exercice.getRepetitions(); 
	}
	
	public int getDureeEnMinutes(){
		return (int) (exercice.getDuree()/60);
	}
	public int getDureeResteEnSeconde(){
		return (int) (exercice.getDuree() % 60);
	}

	public String getType(){
		return exercice.getType();
	}
	
	public Session getSession() {
		return session;
	}
	
	public void retourAccueil(){
		AccueilController AController = new AccueilController(session);
		FenetreAccueil fenetreAccueil = new FenetreAccueil(AController);
		fenetreAccueil.setVisible(true);
	}

	
}
