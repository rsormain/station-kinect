package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;

import java.awt.FontFormatException;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.Box;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.UIManager;
import javax.swing.ImageIcon;

import client.ClientHTTPAuth;

public class FenetreConnexion extends JFrame {

	private JPanel contentPane;
	private JPasswordField motdepasse;
	private JTextField email;

	

	
	public FenetreConnexion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 787, 605);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(153, 204, 102));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 204, 102));
		panel.add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 303, 0};
		gbl_panel_1.rowHeights = new int[]{20, 0, 37, 0, 0, 0, 13, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblEmail = new JLabel(" E-mail :");
		lblEmail.setBackground(new Color(255, 255, 204));
		lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
		lblEmail.setFont(FontesLoader.getFonteTexte((float)30, this));
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.anchor = GridBagConstraints.WEST;
		gbc_lblEmail.insets = new Insets(0, 0, 5, 0);
		gbc_lblEmail.gridx = 1;
		gbc_lblEmail.gridy = 1;
		panel_1.add(lblEmail, gbc_lblEmail);
		
		email = new JTextField();
		email.setFont(new Font("Tahoma", Font.PLAIN, 20));
		email.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_email = new GridBagConstraints();
		gbc_email.fill = GridBagConstraints.HORIZONTAL;
		gbc_email.insets = new Insets(0, 0, 5, 0);
		gbc_email.gridx = 1;
		gbc_email.gridy = 2;
		panel_1.add(email, gbc_email);
		email.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel(" Mot de passe :");
		lblMotDePasse.setFont(FontesLoader.getFonteTexte((float)30, this));
		GridBagConstraints gbc_lblMotDePasse = new GridBagConstraints();
		gbc_lblMotDePasse.insets = new Insets(0, 0, 5, 0);
		gbc_lblMotDePasse.anchor = GridBagConstraints.WEST;
		gbc_lblMotDePasse.gridx = 1;
		gbc_lblMotDePasse.gridy = 3;
		panel_1.add(lblMotDePasse, gbc_lblMotDePasse);
		
		motdepasse = new JPasswordField();
		motdepasse.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_motdepasse = new GridBagConstraints();
		gbc_motdepasse.insets = new Insets(0, 0, 5, 0);
		gbc_motdepasse.fill = GridBagConstraints.HORIZONTAL;
		gbc_motdepasse.gridx = 1;
		gbc_motdepasse.gridy = 4;
		panel_1.add(motdepasse, gbc_motdepasse);
		
		JButton btnConnexion = new JButton("Connexion");
		btnConnexion.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) { //Appui sur le bouton connexion 
				String pass = new String(motdepasse.getPassword());
				String session = ClientHTTPAuth.connection(email.getText(), pass);
				if(session != null ){
					JOptionPane jop1 = new JOptionPane();
					jop1.showMessageDialog(null, "Connection réussie", "Information", JOptionPane.INFORMATION_MESSAGE);
					
					AccueilController AController = new AccueilController(session);
					FenetreAccueil fenetreAccueil = new FenetreAccueil(AController);
					
					fenetreAccueil.setVisible(true);
					setVisible(false);  // on désactive l'affichage de la fenêtre de connection et on libère ses ressources. 
					dispose();
					
				}
				else {
					JOptionPane jop2 = new JOptionPane();
					jop2.showMessageDialog(null, "Echec de la connection", "Information", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		});
		
		
		
		
		btnConnexion.setFont(FontesLoader.getFonteTitre((float)25, this));
		GridBagConstraints gbc_btnConnexion = new GridBagConstraints();
		gbc_btnConnexion.insets = new Insets(0, 0, 5, 0);
		gbc_btnConnexion.anchor = GridBagConstraints.EAST;
		gbc_btnConnexion.gridx = 1;
		gbc_btnConnexion.gridy = 6;
		panel_1.add(btnConnexion, gbc_btnConnexion);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FenetreConnexion.class.getResource("/images/sport.png")));
		panel.add(lblNewLabel);
		
		JLabel lblConnectezvousVotre = new JLabel("Connectez-vous à votre compte FitTogether :");
		lblConnectezvousVotre.setFont(FontesLoader.getFonteTitre((float)40, this));
		contentPane.add(lblConnectezvousVotre, BorderLayout.NORTH);
	}
	
	


}
