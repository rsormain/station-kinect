package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;

import client.ClientHTTPLogout;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FenetreResultats extends JFrame {

	private JPanel contentPane;


	public FenetreResultats(final ResultatsController controller) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.menu);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[69.00,grow][300:333.00,grow][98.00,grow]"));
		
		JLabel lblLexerciceEstTermin = new JLabel("L'exercice est terminé et a bien été enregistré !");
		lblLexerciceEstTermin.setBackground(Color.LIGHT_GRAY);
		lblLexerciceEstTermin.setFont(FontesLoader.getFonteTitre((float)30, this));
		contentPane.add(lblLexerciceEstTermin, "cell 0 0");
		
		JPanel panel = new JPanel();
		contentPane.add(panel, "cell 0 1,grow");
		panel.setLayout(new MigLayout("", "[255.00][grow]", "[grow]"));
		
		JLabel lblNewLabel = new JLabel("");
		
		lblNewLabel.setIcon(new ImageIcon(controller.getLogo(220, FenetreResultats.class.getResource("/images/logo.png"))));
		panel.add(lblNewLabel, "cell 0 0");
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(153, 204, 102));
		panel.add(panel_1, "cell 1 0,grow");
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 43, 0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		Font texte30 = FontesLoader.getFonteTexte((float)30, this);
		
		JLabel lblNewLabel_1 = new JLabel("Vous avez effectué :");
		lblNewLabel_1.setFont(texte30);
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel(controller.getRepetitions() + " "+controller.getType()+" en "+
						controller.getDureeEnMinutes()+" minutes et " + controller.getDureeResteEnSeconde() + " secondes" );
		lblNewLabel_3.setFont(texte30);
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 1;
		panel_1.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		JLabel lblNewLabel_2 = new JLabel("Bravo !");
		lblNewLabel_2.setFont(texte30);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel_1.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblNewLabel_4 = new JLabel(controller.getCoins() + " SportCoins ont été crédités");
		lblNewLabel_4.setFont(texte30);
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 3;
		panel_1.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel(" sur votre compte");
		lblNewLabel_5.setFont(texte30);
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 4;
		panel_1.add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, "flowx,cell 0 2,grow");
		panel_2.setLayout(new MigLayout("", "[grow][grow]", "[grow]"));
		
		JButton btnNewButton = new JButton("Retour à l'accueil");
		btnNewButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				controller.retourAccueil();
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		btnNewButton.setFont(FontesLoader.getFonteTitre((float)25, this));
		panel_2.add(btnNewButton, "cell 0 0,alignx center");
		
		JButton btnNewButton_1 = new JButton("Déconnexion");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				ClientHTTPLogout.deconnection(controller.getSession().getUuid());
				JOptionPane jop1 = new JOptionPane();
				jop1.showMessageDialog(null, "Déconnexion réussie, à bientôt", "Information", JOptionPane.INFORMATION_MESSAGE);
				FenetreConnexion fenetreConnexion = new FenetreConnexion(); //on ouvre une nouvelle fenêtre de connection 
				fenetreConnexion.setVisible(true);
				setVisible(false); // on désactive l'affichage de la fenêtre d'acceuil et on libère ses ressources. 
				dispose();
			}
		});
		btnNewButton_1.setFont(FontesLoader.getFonteTitre((float)25, this));
		panel_2.add(btnNewButton_1, "cell 1 0,alignx center");
	}

}
