package utils;

public class CalculateurSportCoins { // permet de déterminer le nombre de sportcoins gagnés

	private static int tau = 1500;
	
	
	public static int getSportCoins( String type, long duree, int repetitions, long totalSecondes){
		
		float k = getCoeff(type);
		
		int coins = (int) ( repetitions * k * duree * (1+Math.exp( -((double)totalSecondes) / tau)) / 300 );
		
		
		return coins;
	}
	
	private static float getCoeff(String type){
		if(type.equals("ciseaux")){
			return 1;
		}
		if(type.equals("boxe")){
			return 1;
		}
		if(type.equals("squat")){
			return 1.3f;
		}
		if(type.equals("pompes")){
			return 1.7f;
		}
		
		
		return 1;
		
	}
	
}
